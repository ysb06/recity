using ReCity.TerrainSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainTester : MonoBehaviour
{
    public void Test(TerrainPoint point)
    {
        point.SetHeight(2);
    }
}
