using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ReCity.TerrainSystems
{
    public class TerrainTile
    {
        private TerrainSystem terrainRef;
        private Vector2Int tilePosition;

        public TerrainTile(TerrainSystem terrainRef, Vector2Int tile)
        {
            this.terrainRef = terrainRef;
            tilePosition = tile;
        }

        public Vector2Int Position
        {
            get { return tilePosition; }
        }

        public float GetAverageHeight(Vector2Int position)
        {
            int answer = 0;
            TerrainPoint[] points = terrainRef.GetTileCornerPoints(position);
            foreach (TerrainPoint point in points)
            {
                answer += point.Height;
            }
            return answer / points.Length;
        }

        public TerrainPoint[] TileCornerPoints
        {
            get { return terrainRef.GetTileCornerPoints(tilePosition); }
        }

        public Vector3 CenterPosition
        {
            get
            {
                Vector3 value = new Vector3(0.5f, 0f, 0.5f) + terrainRef.transform.position;
                value.x += tilePosition.x;
                value.z += tilePosition.y;
                return value;
            }
        }

        public override string ToString()
        {
            return "Tile " + tilePosition.ToString();
        }
    }
}