using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ReCity.TerrainSystems
{
    [RequireComponent(typeof(Terrain))]
    public class TerrainSystem : MonoBehaviour
    {

        public Terrain TerrainRef { get; private set; }
        public TerrainCollider TerrainRefCollider { get; private set; }
        private readonly Dictionary<(int, int), TerrainPoint> points = new();
        private readonly Dictionary<(int, int), TerrainTile> tiles = new();


        private void Start()
        {
            TerrainRef = GetComponent<Terrain>();
            TerrainRefCollider = GetComponent<TerrainCollider>();
        }

        public TerrainPoint GetPoint(Vector2Int position)
        {
            if (points.TryGetValue((position.x, position.y), out TerrainPoint value))
            {
                return value;
            }
            else
            {
                TerrainPoint point = new TerrainPoint(this, position);
                points.Add((position.x, position.y), point);

                return point;
            }
        }

        public TerrainTile GetTile(Vector2Int position)
        {
            if (tiles.TryGetValue((position.x, position.y), out TerrainTile value))
            {
                return value;
            }
            else
            {
                TerrainTile tile = new TerrainTile(this, position);
                tiles.Add((position.x, position.y), tile);

                return tile;
            }
        }
        // 추후 GetTiles 함수 제공을 고려할 것

        public void SetHeight(Vector2Int position, int height)
        {
            float[,] heights = { { (float)height / TerrainRef.terrainData.heightmapResolution } };
            TerrainRef.terrainData.SetHeights(position.x, position.y, heights);
        }

        public int GetHeight(Vector2Int position)
        {
            float height = TerrainRef.terrainData.GetHeight(position.x, position.y);

            return Mathf.RoundToInt(height);
        }

        public TerrainPoint[] GetTileCornerPoints(Vector2Int position)
        {
            TerrainPoint[] points = new TerrainPoint[4];
            points[0] = GetPoint(position);
            points[1] = GetPoint(new Vector2Int(position.x + 1, position.y));
            points[2] = GetPoint(new Vector2Int(position.x + 1, position.y + 1));
            points[3] = GetPoint(new Vector2Int(position.x, position.y + 1));

            return points;
        }
    }
}