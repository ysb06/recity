using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ReCity.TerrainSystems
{
    public class TerrainPoint
    {
        private TerrainSystem terrainRef;
        private Vector2Int point;

        public Vector2Int Point
        {
            get { return point; }
        }

        public int Height
        {
            get { return terrainRef.GetHeight(Point); }
        }

        public TerrainPoint(TerrainSystem terrainRef, Vector2Int point)
        {
            this.terrainRef = terrainRef;
            this.point = point;
        }

        public void SetHeight(int height)
        {
            terrainRef.SetHeight(point, height);

            // 포인트 기준 상하좌우 높이가 1 이상 차이가 나면 해당 지점 height - 1만큼 조정
        }

        public override string ToString()
        {
            return "Point: " + point.ToString();
        }
    }
}