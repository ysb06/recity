using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace ReCity.StructureSystems
{
    public class StructureEventController : MonoBehaviour
    {
        public Camera currentCamera;
        private int targetLayer = 1;
        private Structure currentStructure = null;
        public StructureEvent StructureHoverEvent = new();
        public StructureEvent StructureSelectedEvent = new();

        private void Start()
        {
            if (currentCamera == null)
                currentCamera = Camera.main;

            targetLayer <<= LayerMask.NameToLayer("Structure");
        }

        private void Update()
        {
            // Hover 이벤트를 쓰지 않을 때에는 오히려 비효율적인 코드임.
            if (EventSystem.current.IsPointerOverGameObject() == false)
            {
                Ray screenRay = currentCamera.ScreenPointToRay(Mouse.current.position.ReadValue());
                if (Physics.Raycast(screenRay, out RaycastHit hit, 100f, targetLayer))
                {
                    Structure obj = hit.collider.GetComponentInParent<Structure>();
                    if (obj != null)
                    {
                        StructureHoverEvent.Invoke(obj);
                        currentStructure = obj;
                        return;
                    }
                }
            }
            currentStructure = null;
        }

        public void OnSelect(InputAction.CallbackContext context)
        {
            // 단, Hover 이벤트를 쓰지 않는다고 해도 여기서 EventSystem.current.IsPointerOverGameObject()를 사용하면 경고창이 뜨므로 이 부분을 해결하는 알고리즘으로 작성 필요
            if (context.phase == InputActionPhase.Canceled && isActiveAndEnabled)
            {
                if (currentStructure != null)
                {
                    StructureSelectedEvent.Invoke(currentStructure);
                    currentStructure = null;
                }
            }
        }
    }

    [Serializable]
    public class StructureEvent : UnityEvent<Structure> { }
}