using ReCity.TerrainSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace ReCity.EventSystems
{
    public class TerrainTileEventController : MonoBehaviour
    {
        public Camera currentCamera;
        public TerrainSystem target;
        private int targetLayer = 1;

        public TerrainTileEvent TerrainTileOverEvent = new();
        public TerrainTileEvent TerrainTileSelectedEvent = new();
        public TerrainPointEvent TerrainPointOverEvent = new();
        public TerrainPointEvent TerrainPointSelectedEvent = new();

        private Vector2Int lastTile = new();
        private Vector2Int lastPoint = new();

        private void Start()
        {
            if (currentCamera == null)
                currentCamera = Camera.main;

            targetLayer <<= LayerMask.NameToLayer("Terrain");
        }

        private void Update()
        {
            if (EventSystem.current.IsPointerOverGameObject() == false)
            {
                Ray screenRay = currentCamera.ScreenPointToRay(Mouse.current.position.ReadValue());
                if (Physics.Raycast(screenRay, out RaycastHit hit, 100f, targetLayer))
                {
                    if (hit.collider == target.TerrainRefCollider)
                    {
                        Vector2Int currentTile = new(Mathf.RoundToInt(hit.point.x - 0.5f), Mathf.RoundToInt(hit.point.z - 0.5f));
                        Vector2Int currentPoint = new(Mathf.RoundToInt(hit.point.x), Mathf.RoundToInt(hit.point.z));

                        if (currentTile != lastTile)
                        {
                            TerrainTile arg = target.GetTile(currentTile);
                            TerrainTileOverEvent.Invoke(arg);

                            lastTile = currentTile;
                        }

                        if (currentPoint != lastPoint)
                        {
                            TerrainPoint arg = target.GetPoint(currentPoint);
                            TerrainPointOverEvent.Invoke(arg);

                            lastPoint = currentPoint;
                        }

                        return;
                    }
                }
            }
        }

        public void Select(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Canceled && isActiveAndEnabled)
            {
                TerrainTile tileArg = target.GetTile(lastTile);
                TerrainTileSelectedEvent.Invoke(tileArg);

                TerrainPoint pointArg = target.GetPoint(lastPoint);
                TerrainPointSelectedEvent.Invoke(pointArg);
            }
        }
    }

    [Serializable]
    public class TerrainTileEvent : UnityEvent<TerrainTile> { }

    [Serializable]
    public class TerrainPointEvent : UnityEvent<TerrainPoint> { }
}