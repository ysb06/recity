using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ReCity.TerrainSystems;

namespace ReCity.StructureSystems {
    public class SimpleStructureBuilder : MonoBehaviour
    {
        public StructureManager Manager;
        public Transform TerrainTransform;
        public Structure Target;

        private void Start()
        {
            // StructureManager �ʱ�ȭ
        }


        public void OnTileSelected(TerrainTile tile)
        {
            if (isActiveAndEnabled)
            {
                GameObject buildingTargetObj = Instantiate(Target.gameObject, TerrainTransform);
                Structure buildingTarget = buildingTargetObj.GetComponent<Structure>();
                try
                {
                    Manager.Build(buildingTarget, tile);
                }
                catch (StructureAlreadyBuiltException e)
                {
                    Debug.LogWarning(e.Message);
                }
                catch (TileAlreadyOccupiedException e)
                {
                    Debug.LogWarning(e.Message);
                }
            }
        }
    }
}