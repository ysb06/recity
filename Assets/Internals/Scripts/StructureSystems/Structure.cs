using ReCity.TerrainSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ReCity.StructureSystems
{
    public class Structure : MonoBehaviour
    {
        // 이 컴포넌트가 있는 Position이 오브젝트의 중심임
        public Vector2Int[] RelativeOccupiedPositons;

        private StructureManager Manager;
        protected TerrainTile centerTile;

        public TerrainTile[] OccupiedTiles { get; protected set; } = new TerrainTile[0];

        [Space(20)]
        public Collider StructureCollider = null;

        private void Start()
        {
            if (StructureCollider == null)
            {
                if (!TryGetComponent(out StructureCollider))
                {
                    StructureCollider = GetComponentInChildren<Collider>();
                }
            }
        }

        public void Initialize(StructureManager manager, TerrainTile center, TerrainTile[] OccupiedPositions)
        {
            Manager = manager;

            centerTile = center;
            transform.position = center.CenterPosition;

            OccupiedTiles = OccupiedPositions;
        }

        private void OnDestroy()
        {
            
        }
    }
}