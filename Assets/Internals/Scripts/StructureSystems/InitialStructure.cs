using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ReCity.TerrainSystems;

namespace ReCity.StructureSystems
{
    public class InitialStructure : Structure
    {
        [Space(20)]
        public Vector2Int InitialPoint;
        public bool InitialPointSpecified = false;

        public void Initialize(Dictionary<TerrainTile, Structure> occupiedTiles)
        {

        }
    }
}
