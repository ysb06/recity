using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ReCity.StructureSystems {
    public class SimpleStructureSelector : MonoBehaviour
    {
        public void OnBuildingSelected(Structure target)
        {
            print($"Structure Selected: {target}");
        }
    }
}