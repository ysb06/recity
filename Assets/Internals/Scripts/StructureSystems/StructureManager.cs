using ReCity.TerrainSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ReCity.StructureSystems
{
    public class StructureAlreadyBuiltException : Exception
    {
        public StructureAlreadyBuiltException(Structure structure) : base($"Manager have the Sructure[{structure.name}] already") { }
    }

    public class TileAlreadyOccupiedException : Exception
    {
        public TileAlreadyOccupiedException(TerrainTile tile) : base($"The tile[{tile.Position}] already") { }
    }

    public class StructureManager : MonoBehaviour
    {
        public TerrainSystem TerrainSystem;
        public Dictionary<Structure, TerrainTile[]> OccupiedTiles = new();
        public Dictionary<TerrainTile, Structure> StructuresOnTile = new();

        private void Start()
        {
            // Terrain System �ʱ�ȭ
        }

        public void Build(Structure structure, TerrainTile center)
        {
            List<TerrainTile> OccupiedTiles = new();
            foreach (Vector2Int position in structure.RelativeOccupiedPositons)
            {
                TerrainTile tile = TerrainSystem.GetTile(position + center.Position);
                OccupiedTiles.Add(tile);
            }

            if (this.OccupiedTiles.ContainsKey(structure))
            {
                throw new StructureAlreadyBuiltException(structure);
            }
            else
                foreach (TerrainTile tile in OccupiedTiles)
                    if (StructuresOnTile.ContainsKey(tile))
                    {
                        throw new TileAlreadyOccupiedException(tile);
                    }

            
            structure.Initialize(this, center, OccupiedTiles.ToArray());

            this.OccupiedTiles.Add(structure, structure.OccupiedTiles);
            foreach (TerrainTile tile in structure.OccupiedTiles)
                StructuresOnTile.Add(tile, structure);
        }
    }
}
