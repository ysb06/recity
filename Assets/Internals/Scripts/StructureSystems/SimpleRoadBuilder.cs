using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ReCity.StructureSystems;
using ReCity.TerrainSystems;

public class SimpleRoadBuilder : MonoBehaviour
{
    public Material RoadMaterial;
    public TerrainTile StartPoint;
    public LineRenderer Visualizer;

    public void OnTileSelected(TerrainTile tile)
    {
        if (isActiveAndEnabled)
        {
            if (StartPoint == null)
            {
                StartPoint = tile;
            }
            else
            {
                BuildRoad(StartPoint, tile);
                StartPoint = null;
            }
        }
    }

    /// <summary>
    /// Build straight road
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    private void BuildRoad(TerrainTile start, TerrainTile end)
    {
        Visualizer.SetPositions(new Vector3[] { start.CenterPosition, end.CenterPosition });

        Vector3 startPoint = start.CenterPosition;
        Vector3 endPoint = end.CenterPosition;
        float roadWidth = 1f; // 도로의 너비
        float roadHeight = 0.1f; // 도로의 높이

        // 도로 길이 계산
        float roadLength = Vector3.Distance(startPoint, endPoint);

        // 도로 메쉬 생성
        GameObject road = new("Road");
        MeshFilter meshFilter = road.AddComponent<MeshFilter>();
        road.AddComponent<MeshRenderer>();

        Mesh mesh = new Mesh();
        Vector3[] vertices = new Vector3[8];

        float halfWidth = roadWidth / 2f;
        float halfLength = roadLength / 2f;

        // 바닥 면의 4개 점
        vertices[0] = new Vector3(-halfWidth, 0f, -halfLength);
        vertices[1] = new Vector3(halfWidth, 0f, -halfLength);
        vertices[2] = new Vector3(halfWidth, 0f, halfLength);
        vertices[3] = new Vector3(-halfWidth, 0f, halfLength);

        // 상단 면의 4개 점
        vertices[4] = new Vector3(-halfWidth, roadHeight, -halfLength);
        vertices[5] = new Vector3(halfWidth, roadHeight, -halfLength);
        vertices[6] = new Vector3(halfWidth, roadHeight, halfLength);
        vertices[7] = new Vector3(-halfWidth, roadHeight, halfLength);

        mesh.vertices = vertices;

        int[] tris = new int[36] {
            // 바닥
            2, 1, 0, 3, 2, 0,
            // 상단
            6, 5, 4, 7, 6, 4,
            // 왼쪽 면
            7, 4, 0, 3, 7, 0,
            // 오른쪽 면
            5, 6, 1, 6, 2, 1,
            // 전면
            5, 1, 0, 4, 5, 0,
            // 후면
            7, 3, 2, 6, 7, 2
        };
        mesh.triangles = tris;

        // UVs
        Vector2[] uvs = new Vector2[vertices.Length];
        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
        }
        mesh.uv = uvs;

        mesh.RecalculateNormals();
        meshFilter.mesh = mesh;

        road.GetComponent<MeshRenderer>().material = RoadMaterial;

        // 도로를 시작점과 끝점 사이에 배치하고 회전시킵니다.
        road.transform.position = startPoint + (endPoint - startPoint) / 2 + new Vector3(0, 0.01f, 0);
        road.transform.up = Vector3.up; // 도로의 "위" 방향을 Y축으로 설정
        road.transform.LookAt(endPoint);
        //road.transform.Rotate(90, 0, 0); // 메쉬를 올바르게 배치하기 위해 회전
    }
}
