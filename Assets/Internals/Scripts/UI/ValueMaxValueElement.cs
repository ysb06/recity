using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ReCity.UI
{
    public class ValueMaxValueElement : MonoBehaviour
    {
        /// <summary>
        /// Temporary Controller for Max Value State
        /// </summary>
        public bool initialMaxValueState = true;

        private Text valueText;
        private Text maxValueText;
        private Text splitter;
        public int Value
        {
            get { return int.Parse(valueText.text); }
            set { valueText.text = value.ToString(); }
        }

        public int MaxValue
        {
            get { return int.Parse(maxValueText.text); }
            set { maxValueText.text = value.ToString(); }
        }

        public bool IsMaxValueVisible
        {
            get { return splitter.gameObject.activeInHierarchy && maxValueText.gameObject.activeInHierarchy; }
            set
            {
                splitter.gameObject.SetActive(value);
                maxValueText.gameObject.SetActive(value);
            }
        }

        private void Start()
        {
            Text[] components = GetComponentsInChildren<Text>();
            foreach (Text text in components)
            {
                switch(text.name)
                {
                    case "Value":
                        valueText = text;
                        break;
                    case "Max Value":
                        maxValueText = text;
                        break;
                    case "Splitter":
                        splitter = text;
                        break;
                    default:
                        Debug.LogWarning("Unknown text ui");
                        break;
                }
            }

            IsMaxValueVisible = initialMaxValueState;
        }
    }
}