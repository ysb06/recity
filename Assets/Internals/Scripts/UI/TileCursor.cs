using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ReCity.TerrainSystems;

namespace ReCity.UI
{
    public class TileCursor : MonoBehaviour
    {
        private LineRenderer cursor;

        private void Start()
        {
            if (cursor == null)
            {
                cursor = GetComponent<LineRenderer>();
            }
        }

        public void Move(TerrainTile tile)
        {
            TerrainPoint[] points = tile.TileCornerPoints;
            Vector3[] positions = new Vector3[5];
            for (int i = 0; i < points.Length; i++)
            {
                positions[i] = new Vector3(points[i].Point.x, points[i].Height, points[i].Point.y);
            }
            positions[4] = new Vector3(points[0].Point.x, points[0].Height, points[0].Point.y);

            cursor.SetPositions(positions);
        }
    }
}