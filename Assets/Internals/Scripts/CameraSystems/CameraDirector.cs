using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Cinemachine;

namespace ReCity.CameraSystems
{
    public class CameraDirector : MonoBehaviour
    {
        public CinemachineOrbitalFollow CinemachineCamera;
        public Camera TargetCamera;

        public float TargetMoveSpeed = 2.5f;
        public float TargetRotateSpeed = 50f;
        public float ZoomScale = 0.1f;

        private Vector2 camMoveDirection = new();
        private float camRotateDirection = 0f;



        private void Start()
        {
            if (CinemachineCamera == null)
            {
                Debug.LogWarning("No Cinemachine Camera");
            }
            if (TargetCamera == null)
            {
                Debug.LogWarning("No Target Camera");
            }
        }

        private void Update()
        {
            Vector3 forward = TargetCamera.transform.forward;
            forward.y = 0;
            forward.Normalize();
            Vector3 right = TargetCamera.transform.right;
            right.y = 0;
            right.Normalize();

            transform.position += TargetMoveSpeed * Time.deltaTime * (forward * camMoveDirection.y + right * camMoveDirection.x);
            CinemachineCamera.HorizontalAxis.Value += TargetRotateSpeed * Time.deltaTime * camRotateDirection;
        }

        public void Zoom(InputAction.CallbackContext context)
        {
            // Cinemachin Input Axis Controller가 동작하지 않아 만든 임시 코드, 나중에 해결 방안 찾아볼 것
            if (context.phase == InputActionPhase.Performed)
            {
                Vector2 value = context.ReadValue<Vector2>();
                CinemachineCamera.RadialAxis.Value += ZoomScale * -value.y;
                CinemachineCamera.RadialAxis.Validate();
            }
        }

        public void Move(InputAction.CallbackContext context)
        {
            switch (context.phase)
            {
                case InputActionPhase.Performed:
                case InputActionPhase.Canceled:
                    camMoveDirection = context.ReadValue<Vector2>();
                    //print($"{context.action}({context.phase}): {value} -> {direction}");
                    break;
            }
        }
        public void Rotate(InputAction.CallbackContext context)
        {
            switch (context.phase)
            {
                case InputActionPhase.Performed:
                case InputActionPhase.Canceled:
                    camRotateDirection = context.ReadValue<float>();
                    //print($"{context.action}({context.phase}): {value}");
                    break;
            }
        }
    }
}
