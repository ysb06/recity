using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GraphRouter.Routers;

[CustomEditor(typeof(Router))]
public class LegacyRouterInspetor : Editor
{
    private void OnEnable()
    {
        Router router = (Router)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Router router = (Router)target;
        if(GUILayout.Button("Initialize"))
        {
            router.Initialize();
        }
        
        if (GUILayout.Button("Play"))
        {
            router.enabled = true;
        }
    }
}
