using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QuikGraph;
using QuikGraph.Collections;
using GraphRouter.Edges;

namespace GraphRouter
{
    public class Node : MonoBehaviour
    {
        public Vector3 Position { 
            get { return transform.position; }
            set { transform.position = value; }
        }

        public HashSet<Edge> InEdges { get; private set; } = new HashSet<Edge>();
        public HashSet<Edge> OutEdges { get; private set; } = new HashSet<Edge>();

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            Gizmos.DrawSphere(transform.position, 0.1f);

            foreach(Edge edge in OutEdges)
            {
                for (int i = 0; i < edge.PointCount - 1; i++)
                {
                    if (i != edge.PointCount - 1)
                    {
                        Gizmos.DrawLine(edge[i], edge[i + 1]);
                    }
                    else
                    {
                        Vector3 adj = (edge[i] - edge[i + 1]).normalized / 10;
                        Gizmos.DrawLine(edge[i], edge[i + 1] + adj);
                    }
                }
            }
        }
    }
}