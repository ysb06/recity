using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GraphRouter.Routers
{
    public class BasicRouter : Router
    {
        public float Speed = 1f;
        protected override bool NotifiyArrived(Vector3 current, Vector3 target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Speed * Time.deltaTime);

            return current == target;
        }
    }
}