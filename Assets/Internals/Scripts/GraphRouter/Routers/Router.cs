using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GraphRouter.Edges;

namespace GraphRouter.Routers
{
    public abstract class Router : MonoBehaviour
    {
        private IEnumerable<Edge> routeOrigin = null;
        private Queue<Edge> route = new Queue<Edge>();
        private Queue<Vector3> points = new Queue<Vector3>();
        public bool Loop = false;
        public bool AutoDirection = true;

        public Edge CurrentEdge = null;
        public Vector3 CurrentTarget = new Vector3();

        public virtual void Initialize()
        {
            route = new Queue<Edge>(routeOrigin);
            SetNextTarget();
        }

        protected abstract bool NotifiyArrived(Vector3 current, Vector3 target);

        public void SetRoute(IEnumerable<Edge> edges)
        {
            routeOrigin = edges;
        }

        protected void SetNextTarget()
        {
            if (points.Count > 0)
            {
                CurrentTarget = points.Dequeue();
            }
            else if (route.Count > 0)
            {
                CurrentEdge = route.Dequeue();
                points = new Queue<Vector3>(CurrentEdge);
                CurrentTarget = points.Dequeue();
            }
            else if (Loop) {
                route = new Queue<Edge>(routeOrigin);
            }

            if (AutoDirection)
            {
                transform.LookAt(CurrentEdge[CurrentEdge.PointCount - 1]);
            }
        }

        private void Update()
        {
            if (NotifiyArrived(transform.position, CurrentTarget))
            {
                SetNextTarget();
            }
        }
    }
}