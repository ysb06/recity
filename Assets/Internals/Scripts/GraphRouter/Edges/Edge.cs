using System.Collections.Generic;
using UnityEngine;
using QuikGraph;
using System.Collections;

namespace GraphRouter.Edges
{
    public class Edge : IEdge<Node>, IEnumerable<Vector3>
    {
        public Node Source { get; protected set; }
        public Node Target { get; protected set; }

        public float Distance
        {
            get
            {
                return Vector3.Distance(Source.Position, Target.Position);
            }
        }
        protected Vector3[] Checkpoints;

        public Edge(Node origin, Node target, Vector3[] checkpoints)
        {
            Source = origin;
            Source.OutEdges.Add(this);
            Target = target;
            Target.InEdges.Add(this);

            List<Vector3> temp = new List<Vector3>(checkpoints.Length + 2);
            temp.Add(Source.Position);
            temp.AddRange(checkpoints);
            temp.Add(Target.Position);

            Checkpoints = temp.ToArray();
        }

        public Edge(Node origin, Node target) : this(origin, target, new Vector3[0]) { }

        public virtual Vector3 this[int index]
        {
            get { return Checkpoints[index]; }
        }

        public virtual int PointCount { get { return Checkpoints.Length; } }

        public void Destroy()
        {
            Source.OutEdges.Remove(this);
            Target.InEdges.Remove(this);
        }

        public IEnumerator<Vector3> GetEnumerator()
        {
            return ((IEnumerable<Vector3>)Checkpoints).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Checkpoints.GetEnumerator();
        }

        public override string ToString()
        {
            return $"{Source.Position}--{Target.Position}";
        }
    }
}