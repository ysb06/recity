﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GraphRouter.Edges
{
    public class PointEdge : Edge
    {
        private Vector3[] points;

        public PointEdge(Node origin, Node target) : base(origin, target)
        {
            Vector3[] temp = { origin.Position, target.Position };
            points = temp;
        }

        public PointEdge(Node origin, Node target, Vector3[] checkpoints) : base(origin, target, checkpoints)
        {
            List<Vector3> points = new List<Vector3>();
            points.Add(Source.transform.position);
            points.AddRange(checkpoints);
            points.Add(Target.transform.position);

            this.points = points.ToArray();
        }

        public override Vector3 this[int index] => points[index];

        public override int PointCount => points.Length;
    }
}
