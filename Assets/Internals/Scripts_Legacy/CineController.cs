using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Composites;

namespace ReCity.UI
{
    public class CineController : MonoBehaviour
    {
        public float rotateSpeed = 0.75f;
        public float rotateValue = 0;
        public float zoomSpeed = 10f;
        public float zoomValue = 0;

        private void Start()
        {

        }

        public void ZoomCamera(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Performed)
            {
                //CameraController.enabled = true;
                zoomValue = context.ReadValue<float>();
            }
            else
            {
                zoomValue = 0;
            }
        }

        public void RotateCamera(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Performed)
            {
                //CameraController.enabled = true;
                rotateValue = context.ReadValue<float>();
            }
            else
            {
                rotateValue = 0;
            }
        }

        private void Update()
        {
            if (rotateValue > 0.1f || rotateValue < -0.1f)
            {
                //CameraController.m_XAxis.m_InputAxisValue = -rotateSpeed * rotateValue;
            }
            else
            {
                //CameraController.m_XAxis.m_InputAxisValue = 0;
            }

            if (zoomValue > 0.1f || zoomValue < -0.1f)
            {
                //CameraController.m_YAxis.m_InputAxisValue = zoomSpeed * (zoomValue >= 0 ? -1 : 1);
            }
            else
            {
                //CameraController.m_YAxis.m_InputAxisValue = 0;
            }
        }
    }
}