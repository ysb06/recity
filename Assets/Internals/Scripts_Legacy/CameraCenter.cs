using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace ReCity.UI
{
    public class CameraCenter : MonoBehaviour
    {
        public Camera target;
        private Vector3 speed = new();
        private Vector3 forward = new();
        public float Speed = .01f;

        private void Start()
        {
            if (target == null)
            {
                target = Camera.main;
            }
        }

        public void MoveCamera(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Started)
            {
                forward = target.transform.forward;
                forward.y = 0;
            }
            else if (context.phase == InputActionPhase.Performed)
            {
                Vector2 value = context.ReadValue<Vector2>();
                speed = new Vector3(value.x, 0, value.y).normalized;
            }
            else
            {
                speed = new Vector3();
            }
        }

        private void Update()
        {
            Quaternion rot = Quaternion.FromToRotation(Vector3.forward, forward);
            transform.position = Vector3.MoveTowards(transform.position, transform.position + (rot * speed), Speed * Time.deltaTime);
        }
    }
}