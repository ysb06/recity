﻿External Asset List
=============

## Images

* https://www.gamedevmarket.net/asset/game-ui-9589/
* https://www.gamedevmarket.net/asset/vector-game-ui-9588/

## Packages

### POLYGON City - Low Poly 3D Art by Synty
* https://assetstore.unity.com/packages/3d/environments/urban/polygon-city-low-poly-3d-art-by-synty-95214

### Simple Military - Cartoon War
* https://assetstore.unity.com/packages/3d/characters/simple-military-cartoon-war-34497

### Outline Effect
* https://assetstore.unity.com/packages/vfx/shaders/fullscreen-camera-effects/outline-effect-78608

### Easy Road 3D
* https://assetstore.unity.com/packages/2d/textures-materials/roads/easyroads3d-pro-add-on-hd-roads-118774

### Bézier Path Creator
* https://assetstore.unity.com/packages/tools/utilities/b-zier-path-creator-136082#description